//
//  TabbarController.swift
//  MandiriTMDB
//
//  Created by Benaya Oktavianus on 03/02/24.
//

import UIKit
import ESTabBarController

final class TabbarProvider {
    static func provideTabbar() -> ESTabBarController {
        let tabbar = ESTabBarController()
        
        let home = UINavigationController(rootViewController: HomeRouter.createModule() as! HomeViewController)
        let discover = UINavigationController(rootViewController: GenreListRouter.createModule() as! GenreListViewController)
        
        home.tabBarItem = ESTabBarItem(title: "Home", image: UIImage(systemName: "house"), selectedImage: UIImage(systemName: "house.fill"))
        
        discover.tabBarItem = ESTabBarItem(title: "Discover", image: UIImage(systemName: "magnifyingglass"), selectedImage: UIImage(systemName: "magnifyingglass.circle.fill.fill"))
        
        tabbar.viewControllers = [home, discover]
        tabbar.tabBar.barTintColor = .white
        return tabbar
        
    }
}
