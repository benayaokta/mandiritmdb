//
//  DialogHelper.swift
//  MandiriTMDB
//
//  Created by Benaya Oktavianus on 04/02/24.
//

import UIKit

class DialogHelper {
    static func showAlert(title: String, description: String, viewController: UIViewController) {
        let alertController = UIAlertController(title: title, message: description, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default) { _ in
            alertController.dismiss(animated: true, completion: nil)
        }
        alertController.addAction(okAction)
        viewController.present(alertController, animated: true, completion: nil)
    }
}
