//
//  APIManager.swift
//  MandiriTMDB
//
//  Created by Benaya Oktavianus on 03/02/24.
//

import Foundation
import Alamofire

final class APIManager {
    static let shared: APIManager = APIManager()
    private init() { }
    
    func request<T: Decodable>(config: APIConfigProtocol, 
                               model: T.Type,
                               completion: @escaping (Result<T, Error>) -> Void) {
        AF.request(config)
            .validate()
            .responseDecodable(of: T.self,
                               queue: DispatchQueue.global(qos: .background)
            ) { response in
                switch response.result {
                case .success(let value):
                    completion(.success(value))
                case .failure(let value):
                    completion(.failure(value))
                }
            }
    }
}
