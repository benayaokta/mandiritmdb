//
//  APIConfig.swift
//  MandiriTMDB
//
//  Created by Benaya Oktavianus on 03/02/24.
//

import Foundation
import Alamofire

protocol APIConfigProtocol: URLRequestConvertible {
    var baseURL: String { get }
    var path: String { get }
    var httpMethod: HTTPMethod { get }
    var paramters: Parameters? { get }
    var jsonEncoding: URLEncoding { get }
    
}

extension APIConfigProtocol {
    
    var baseURL: String {
        return "https://api.themoviedb.org/3"
    }
    
    
    func asURLRequest() throws -> URLRequest {
        let url = try self.baseURL.asURL().appendingPathComponent(path)
        
        // Create the URLRequest
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = httpMethod.rawValue
        
        // Set additional headers if needed
        urlRequest.setValue("application/json", forHTTPHeaderField: "accept")
        urlRequest.setValue("Bearer eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiI3ODJjOTlhMTA4NmVmNTA0YjgxOGUwODFjM2I1OWFmOCIsInN1YiI6IjY1YmQxMjM1NDU5YWQ2MDE3YTZlNmMzNiIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.u6FEWXXGZGsslqdYEaXufYucnE1XlWPDj2N8qtqtV9c", forHTTPHeaderField: "Authorization")
        
        if let parameters = paramters {
            urlRequest = try URLEncoding.default.encode(urlRequest, with: parameters)
        }
        
        return urlRequest
        
    }
}
