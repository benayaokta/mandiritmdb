//
//  GenreTableViewCell.swift
//  MandiriTMDB
//
//  Created by Benaya Oktavianus on 04/02/24.
//

import UIKit
import Stevia

final class GenreTableViewCell: UITableViewCell {
    
    private let genreText: UILabel = UILabel()
    
    required init?(coder: NSCoder) {
        fatalError("coder")
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        commonInit()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        setupStyle()
    }
    
    private func commonInit() {
        setupHierarchy()
        setupConstraint()
        setupStyle()
    }
    
    private func setupHierarchy() {
        self.subviews {
            genreText
        }
    }
    
    private func setupConstraint() {
        genreText.centerHorizontally().centerVertically()
    }
    
    private func setupStyle() {
        self.backgroundColor = .white
        genreText.font = UIFont.systemFont(ofSize: 16, weight: .semibold)
        genreText.textColor = .black
    }
    
    func setGenre(text: String) {
        self.genreText.text = text
    }

}
