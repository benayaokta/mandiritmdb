//
//  ReviewTableViewCell.swift
//  MandiriTMDB
//
//  Created by Benaya Oktavianus on 04/02/24.
//

import UIKit
import Stevia
import SDWebImage

final class ReviewTableViewCell: UITableViewCell {
    private let authorName: UILabel = UILabel()
    private let authorImage: UIImageView = UIImageView()
    private let authorComment: UILabel = UILabel()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        fatalError("fatal error")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        setupStyle()
    }
    
    private func commonInit() {
        setupHierarchy()
        setupConstraint()
        setupStyle()
    }
    
    private func setupHierarchy() {
        self.subviews {
            authorComment
            authorImage
            authorName
        }
    }
    
    private func setupConstraint() {
        authorComment.fillHorizontally(padding: 16).Top == self.Top + 8
        authorImage.Leading == authorComment.Leading
        authorImage.width(20).heightEqualsWidth().Top == authorComment.Bottom + 8
        authorImage.Bottom == self.Bottom - 8
        
        authorName.Leading == authorImage.Trailing + 8
        authorName.Top == authorImage.Top
        authorName.Trailing == self.Trailing - 8
        authorName.Bottom == self.Bottom - 8
    }
    
    private func setupStyle() {
        authorName.font = UIFont.systemFont(ofSize: 9, weight: .semibold)
        authorName.textColor = .black
        authorImage.layer.cornerRadius = 20
        authorComment.font = UIFont.systemFont(ofSize: 14)
        authorComment.textColor = .black
        authorComment.adjustsFontSizeToFitWidth = true
        authorComment.minimumScaleFactor = 0.8
        authorComment.numberOfLines = 0
    }
    
    func setImageAuthor(url: String) {
        authorImage.sd_imageIndicator = SDWebImageActivityIndicator()
        let urlString = "https://image.tmdb.org/t/p/w200\(url)"
        authorImage.sd_setImage(with: URL(string: urlString), placeholderImage: UIImage(systemName: "photo"), options: [.progressiveLoad])
    }
    
    func setAuthorComment(text: String) {
        authorComment.text = text
    }
    
    func setAuthorName(text: String) {
        authorName.text = text
    }
}
