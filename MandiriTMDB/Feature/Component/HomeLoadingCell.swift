//
//  HomeLoadingCell.swift
//  MandiriTMDB
//
//  Created by Benaya Oktavianus on 03/02/24.
//

import UIKit
import Stevia

final class HomeLoadingCell: UITableViewCell {
    
    private let indicator: UIActivityIndicatorView = UIActivityIndicatorView(frame: .zero)
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        commonInit()
        setStyle()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init coder")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        setStyle()
    }
    
    private func commonInit() {
        self.subviews {
            indicator
        }
        
        indicator.centerHorizontally().centerVertically().width(40).heightEqualsWidth()
        
    }
    
    private func setStyle() {
        indicator.color = .black
    }
}
