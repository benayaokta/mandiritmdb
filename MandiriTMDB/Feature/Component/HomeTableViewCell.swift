//
//  HomeTableViewCell.swift
//  MandiriTMDB
//
//  Created by Benaya Oktavianus on 02/02/24.
//

import UIKit
import Stevia
import SDWebImage

final class HomeTableViewCell: UITableViewCell {
    private let movieImage: UIImageView = UIImageView()
    private let movieName: UILabel = UILabel()
    private let movieDesc: UILabel = UILabel()
    
    required init?(coder: NSCoder) {
        fatalError("init coder has not been implemented")
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        commonInit()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        setStyle()
    }
    
    private func commonInit() {
        setupHierarchy()
        setupConstraint()
        setStyle()
    }
    
    private func setupHierarchy() {
        self.subviews {
            movieImage
            movieName
            movieDesc
        }
    }
    
    private func setupConstraint() {
        movieImage.fillVertically(padding: 8).width(70).Leading == self.layoutMarginsGuide.Leading
        
        movieName.Top == movieImage.Top + 1
        movieName.Leading == movieImage.Trailing + 8
        movieName.Trailing == self.safeAreaLayoutGuide.Trailing
        
        movieDesc.Trailing == movieName.Trailing
        movieDesc.Bottom == movieImage.Bottom
        movieDesc.Top == movieName.Bottom
        movieDesc.Leading == movieName.Leading
    }
    
    private func setStyle() {
        self.backgroundColor = .white
        self.movieName.textColor = .black
        self.movieName.font = UIFont.systemFont(ofSize: 16, weight: .semibold)
        self.movieName.numberOfLines = 0
        
        self.movieDesc.font = UIFont.systemFont(ofSize: 14, weight: .regular)
        self.movieDesc.textColor = .black
        self.movieDesc.numberOfLines = 0
    }
    
    func setMovie(name: String) {
        self.movieName.text = name
    }
    
    func setMovie(desc: String) {
        self.movieDesc.text = desc
    }
    
    func setImage(url string: String) {
        self.movieImage.sd_imageIndicator = SDWebImageProgressIndicator()
        let urlString = "https://image.tmdb.org/t/p/w200\(string)"
        self.movieImage.sd_setImage(with: URL(string: urlString), placeholderImage: UIImage(systemName: "photo.fill"), options: [.progressiveLoad])
    }
    
}
