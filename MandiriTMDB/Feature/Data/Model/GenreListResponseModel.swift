//
//  GenreListResponseModel.swift
//  MandiriTMDB
//
//  Created by Benaya Oktavianus on 04/02/24.
//

import Foundation

struct GenreListResponseModel: Codable {
    let genres: [GenreDetailList]?
}

struct GenreDetailList: Codable {
    let id: Int?
    let name: String?
}
