//
//  HomeAPI.swift
//  MandiriTMDB
//
//  Created by Benaya Oktavianus on 03/02/24.
//

import Foundation
import Alamofire

enum CommonAPI: APIConfigProtocol {

    case discover(page: Int)
    case detail(id: Int)
    case getReview(id: Int)
    case getVideoDetail(id: Int)
    case getGenre
    case getMovieListFromGenre(genre: Int, page: Int)
    
    var path: String {
        switch self {
        case .discover, .getMovieListFromGenre:
            return "/discover/movie"
        case .detail(let id):
            return "/movie/\(id)"
        case .getReview(let id):
            return "/movie/\(id)/reviews"
        case .getVideoDetail(let id):
            return "/movie/\(id)/videos"
        case .getGenre:
            return "/genre/movie/list"
        }
    }
    
    var httpMethod: HTTPMethod {
        return .get
    }
    
    var paramters: Parameters? {
        switch self {
        case .discover(let page):
            return ["page": page, "include_adult": false, "include_video": false]
        case .getMovieListFromGenre(let genre, let page):
            return ["include_adult": false, "include_video": false,"with_genres": genre, "page": page]
        default: return nil
        }
    }
    
    var jsonEncoding: URLEncoding {
        return URLEncoding(destination: .queryString)
    }

}
