//
//  HomeRepo.swift
//  MandiriTMDB
//
//  Created by Benaya Oktavianus on 03/02/24.
//

import Foundation

protocol CommonRepo {
    func getMovieDiscoverList(page: Int, result: @escaping (Result<DiscoverResponseModel, Error>) -> Void)
    func getMovieDetail(id: Int, result: @escaping (Result<MovieDetailResponseModel, Error>) -> Void)
    func getMovieReviews(id: Int, result: @escaping (Result<ReviewResponseModel, Error>) -> Void)
    func getMovieVideo(id: Int, result: @escaping (Result<MovieVideoResponseModel, Error>) -> Void)
    
    func getGenreMovieList(result: @escaping (Result<GenreListResponseModel, Error>) -> Void)
    func getMovieFromGenre(id: Int, page: Int, result: @escaping (Result<DiscoverResponseModel, Error>) -> Void)
}
