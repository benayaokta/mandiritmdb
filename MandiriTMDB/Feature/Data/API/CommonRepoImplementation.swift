//
//  HomeRepoImplementation.swift
//  MandiriTMDB
//
//  Created by Benaya Oktavianus on 03/02/24.
//

import Foundation

final class CommonRepoImplementation: CommonRepo {
    
    let manager: APIManager
    init(manager: APIManager = APIManager.shared) {
        self.manager = manager
    }
    
    func getMovieDiscoverList(page: Int, result: @escaping (Result<DiscoverResponseModel, Error>) -> Void) {
        manager.request(config: CommonAPI.discover(page: page), model: DiscoverResponseModel.self, completion: result)
    }
    
    func getMovieDetail(id: Int, result: @escaping (Result<MovieDetailResponseModel, Error>) -> Void) {
        manager.request(config: CommonAPI.detail(id: id), model: MovieDetailResponseModel.self, completion: result)
    }
    
    func getMovieReviews(id: Int, result: @escaping (Result<ReviewResponseModel, Error>) -> Void) {
        manager.request(config: CommonAPI.getReview(id: id), model: ReviewResponseModel.self, completion: result)
    }
    
    func getMovieVideo(id: Int, result: @escaping (Result<MovieVideoResponseModel, Error>) -> Void) {
        manager.request(config: CommonAPI.getVideoDetail(id: id), model: MovieVideoResponseModel.self, completion: result)
    }
    
    func getGenreMovieList(result: @escaping (Result<GenreListResponseModel, Error>) -> Void) {
        manager.request(config: CommonAPI.getGenre, model: GenreListResponseModel.self, completion: result)
    }
    
    func getMovieFromGenre(id: Int, page: Int, result: @escaping (Result<DiscoverResponseModel, Error>) -> Void) {
        manager.request(config: CommonAPI.getMovieListFromGenre(genre: id, page: page), model: DiscoverResponseModel.self, completion: result)
    }
}
