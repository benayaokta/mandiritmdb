//
//  HomeContract.swift
//  MandiriTMDB
//
//  Created by Benaya Oktavianus on 02/02/24.
//  
//

import Foundation
import UIKit.UIViewController

// MARK: View Output (Presenter -> View)
protocol PresenterToViewHomeProtocol {
    func reloadTable()
    func showLoading()
    func hideLoading()
    func showError(message: String)
    func toggleIsLoading(bool: Bool)
}


// MARK: View Input (View -> Presenter)
protocol ViewToPresenterHomeProtocol {
    
    var view: PresenterToViewHomeProtocol? { get set }
    var interactor: PresenterToInteractorHomeProtocol? { get set }
    var router: PresenterToRouterHomeProtocol? { get set }
    
    var movieList: [ResultEntity] { get }
    
    func getMovieList(page: Int, infiniteScroll: Bool)
    func moveToDetail(index: Int)
}


// MARK: Interactor Input (Presenter -> Interactor)
protocol PresenterToInteractorHomeProtocol {
    
    var presenter: InteractorToPresenterHomeProtocol? { get set }
    var movieList: [ResultEntity] { get set }
    
    func getMovie(page: Int)
}


// MARK: Interactor Output (Interactor -> Presenter)
protocol InteractorToPresenterHomeProtocol {
    func successGetMovieDiscoverList()
    func failGetMovieDiscoverList(error message: String)
    
    func showLoading()
    func hideLoading()
}


// MARK: Router Input (Presenter -> Router)
protocol PresenterToRouterHomeProtocol {
    var routerView: UIViewController? { get set }
    func moveToMovieDetail(id: Int)
}
