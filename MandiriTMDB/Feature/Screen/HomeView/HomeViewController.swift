//
//  HomeViewController.swift
//  MandiriTMDB
//
//  Created by Benaya Oktavianus on 02/02/24.
//  
//

import UIKit
import Stevia
import CoreExtension

class HomeViewController: BaseViewController {
    
    var presenter: ViewToPresenterHomeProtocol?
    
    private let tableView: UITableView = UITableView()
    private var isLoading: Bool = false
    private var page: Int = 1
    private let loadingIndicator: UIActivityIndicatorView = UIActivityIndicatorView(frame: .zero)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupHierarchy()
        setupConstraint()
        setupStyle()
        setupActions()
        setupTableView()
        presenter?.getMovieList(page: page, infiniteScroll: false)
    }

    private func setupHierarchy() {
        self.view.subviews {
            tableView
            loadingIndicator
        }
    }
    
    private func setupConstraint() {
        tableView.fillHorizontally().Top == self.view.safeAreaLayoutGuide.Top
        tableView.Bottom == self.view.safeAreaLayoutGuide.Bottom
        
        loadingIndicator.centerInContainer().width(40).heightEqualsWidth()
    }
    
    private func setupStyle() {
        self.title = "Home"
        loadingIndicator.isHidden = true
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.tintColor = .black
    }
    
    private func setupActions() {
        
    }
    
    private func setupTableView() {
        tableView.backgroundColor = .white
        tableView.registerCellClass(type: HomeTableViewCell.self)
        tableView.registerCellClass(type: HomeLoadingCell.self)
        tableView.delegate = self
        tableView.dataSource = self
    }
    
}

extension HomeViewController: PresenterToViewHomeProtocol{
    func reloadTable() {
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    func showLoading() {
        DispatchQueue.main.async { [self] in
            loadingIndicator.startAnimating()
            loadingIndicator.isHidden = false
        }
    }
    
    func hideLoading() {
        DispatchQueue.main.async { [self] in
            loadingIndicator.isHidden = true
            loadingIndicator.stopAnimating()
        }
    }
    
    func showError(message: String) {
        DialogHelper.showAlert(title: "Error", description: message, viewController: self)
    }
    
    func toggleIsLoading(bool: Bool) {
        self.isLoading = bool
    }
    
}

extension HomeViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if isLoading {
            let cell = tableView.dequeueCell(withType: HomeLoadingCell.self, for: indexPath) as! HomeLoadingCell
            return cell
        } else {
            let cell = tableView.dequeueCell(withType: HomeTableViewCell.self, for: indexPath) as! HomeTableViewCell
            let data = presenter?.movieList[safe: indexPath.row]
            cell.setMovie(name: data?.originalTitle ?? "")
            cell.setMovie(desc: data?.overview ?? "")
            cell.setImage(url: data?.posterPath ?? "")
            return cell
        }
    }
}


extension HomeViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return isLoading ? (presenter?.movieList.count ?? 0) + 1 : presenter?.movieList.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        presenter?.moveToDetail(index: indexPath.row)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastRowIndex = tableView.numberOfRows(inSection: 0) - 1
        if indexPath.row == lastRowIndex && !isLoading {
            // Load more data when the last cell is about to be displayed
            isLoading = true
            self.reloadTable()
            page += 1
            presenter?.getMovieList(page: page, infiniteScroll: true)
        }
    }
}
