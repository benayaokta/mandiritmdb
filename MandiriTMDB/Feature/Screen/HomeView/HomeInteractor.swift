//
//  HomeInteractor.swift
//  MandiriTMDB
//
//  Created by Benaya Oktavianus on 02/02/24.
//  
//

import Foundation

class HomeInteractor: PresenterToInteractorHomeProtocol {

    var presenter: InteractorToPresenterHomeProtocol?
    var movieList: [ResultEntity] = []

    let repo: CommonRepo
    init(repo: CommonRepo) {
        self.repo = repo
    }
    
    func getMovie(page: Int) {
        presenter?.showLoading()
        repo.getMovieDiscoverList(page: page) { [weak self] result in
            switch result {
            case .success(let value):
                self?.movieList.append(contentsOf: DiscoverEntity.mapFromDiscoverData(response: value).results)
                self?.presenter?.successGetMovieDiscoverList()
                self?.presenter?.hideLoading()
            case .failure(let failure):
                self?.presenter?.hideLoading()
                self?.presenter?.failGetMovieDiscoverList(error: failure.localizedDescription)
            }
        }
    }
}
