//
//  HomeRouter.swift
//  MandiriTMDB
//
//  Created by Benaya Oktavianus on 02/02/24.
//  
//

import Foundation
import UIKit

class HomeRouter: PresenterToRouterHomeProtocol {
    
    weak var routerView: UIViewController?
    
    // MARK: Static methods
    static func createModule() -> UIViewController {
        
        let viewController = HomeViewController()
        
        let presenter: ViewToPresenterHomeProtocol & InteractorToPresenterHomeProtocol = HomePresenter()
        
        viewController.presenter = presenter
        viewController.presenter?.router = HomeRouter()
        viewController.presenter?.router?.routerView = viewController
        viewController.presenter?.view = viewController
        viewController.presenter?.interactor = HomeInteractor(repo: CommonRepoImplementation())
        viewController.presenter?.interactor?.presenter = presenter
        
        return viewController
    }
    
    func moveToMovieDetail(id: Int) {
        let detail = MovieDetailRouter.createModule(id: id)
        routerView?.navigationController?.pushViewController(detail, animated: true)
    }
    
}
