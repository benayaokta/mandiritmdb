//
//  HomePresenter.swift
//  MandiriTMDB
//
//  Created by Benaya Oktavianus on 02/02/24.
//  
//

import Foundation

class HomePresenter: ViewToPresenterHomeProtocol {

    // MARK: Properties
    var view: PresenterToViewHomeProtocol?
    var interactor: PresenterToInteractorHomeProtocol?
    var router: PresenterToRouterHomeProtocol?
    private var fromInifiniteScroll: Bool = false
    
    var movieList: [ResultEntity] {
        guard let interactor else { return [] }
        return interactor.movieList
    }
    
    func getMovieList(page: Int, infiniteScroll: Bool) {
        self.fromInifiniteScroll = infiniteScroll
        interactor?.getMovie(page: page)
    }
    
    func moveToDetail(index: Int) {
        router?.moveToMovieDetail(id: movieList[index].id)
    }

}

extension HomePresenter: InteractorToPresenterHomeProtocol {
    func showLoading() {
        view?.showLoading()
    }
    
    func hideLoading() {
        view?.hideLoading()
    }
    
    func successGetMovieDiscoverList() {
        if fromInifiniteScroll {
            view?.toggleIsLoading(bool: false)
            fromInifiniteScroll = false
        }
        
        view?.reloadTable()
    }
    
    func failGetMovieDiscoverList(error message: String) {
        view?.showError(message: message)
        
        if fromInifiniteScroll {
            view?.toggleIsLoading(bool: false)
            fromInifiniteScroll = false
        }
    }
}
