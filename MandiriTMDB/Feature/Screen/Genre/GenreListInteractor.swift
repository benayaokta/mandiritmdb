//
//  GenreListInteractor.swift
//  MandiriTMDB
//
//  Created by Benaya Oktavianus on 04/02/24.
//  
//

import Foundation

class GenreListInteractor: PresenterToInteractorGenreListProtocol {

    // MARK: Properties
    var presenter: InteractorToPresenterGenreListProtocol?
    
    var genreEntity: [GenreEntity] = []
    
    let repo: CommonRepo
    init(repo: CommonRepo) {
        self.repo = repo
    }
    
    func getMovieGenre() {
        self.genreEntity.removeAll()
        repo.getGenreMovieList { [weak self] response in
            switch response {
            case .success(let success):
                let entity = success.toDomain().genres
                self?.genreEntity.append(contentsOf: entity)
                self?.presenter?.successGetGenre()
            case .failure(let failure):
                self?.presenter?.failGetGenre(error: failure.localizedDescription)
            }
        }
    }
}
