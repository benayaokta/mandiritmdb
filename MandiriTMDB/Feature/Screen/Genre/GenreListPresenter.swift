//
//  GenreListPresenter.swift
//  MandiriTMDB
//
//  Created by Benaya Oktavianus on 04/02/24.
//  
//

import Foundation

class GenreListPresenter: ViewToPresenterGenreListProtocol {

    // MARK: Properties
    var view: PresenterToViewGenreListProtocol?
    var interactor: PresenterToInteractorGenreListProtocol?
    var router: PresenterToRouterGenreListProtocol?
    
    var genreEntity: [GenreEntity] {
        guard let interactor else { return [GenreEntity]() }
        return interactor.genreEntity
    }
    

    func getMovieGenre() {
        view?.showLoading()
        interactor?.getMovieGenre()
    }
}

extension GenreListPresenter: InteractorToPresenterGenreListProtocol {
    func successGetGenre() {
        view?.hideLoading()
        view?.successGetMovieGenre()
    }
    
    func failGetGenre(error: String) {
        view?.hideLoading()
        view?.failGetMovieGenre(error: error)
    }
    
    func moveToMovieListByGenre(id: Int, title: String) {
        router?.moveToMovieList(id: id, title: title)
    }
     
}
