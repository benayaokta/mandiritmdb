//
//  GenreListViewController.swift
//  MandiriTMDB
//
//  Created by Benaya Oktavianus on 04/02/24.
//  
//

import UIKit
import Stevia

class GenreListViewController: BaseViewController {
    
    var presenter: ViewToPresenterGenreListProtocol?
    
    private let tableView: UITableView = UITableView()
    private let refreshControl: UIRefreshControl = UIRefreshControl()
    private let loadingIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupHierarchy()
        setupConstraint()
        setupTableView()
        setupStyle()
        setupActions()
        presenter?.getMovieGenre()
    }
    
    private func setupHierarchy() {
        self.view.subviews {
            tableView
            loadingIndicator
        }
    }
    
    private func setupConstraint() {
        tableView.fillHorizontally().Top == self.view.safeAreaLayoutGuide.Top
        tableView.Bottom == self.view.safeAreaLayoutGuide.Bottom
        tableView.addSubview(refreshControl)
        loadingIndicator.centerInContainer().width(40).heightEqualsWidth()
    }
    
    private func setupStyle() {
        self.title = "Discover Genre"
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        loadingIndicator.hidesWhenStopped = true
    }
    
    private func setupActions() {
        refreshControl.addTarget(self, action: #selector(pullToRefresh), for: .valueChanged)
    }

    private func setupTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.registerCellClass(type: GenreTableViewCell.self)
    }
    
    @objc private func pullToRefresh() {
        presenter?.getMovieGenre()
    }
    
}

extension GenreListViewController: PresenterToViewGenreListProtocol{
    func showLoading() {
        DispatchQueue.main.async { [self] in
            loadingIndicator.startAnimating()
            refreshControl.beginRefreshing()
        }
    }
    
    func hideLoading() {
        DispatchQueue.main.async { [self] in
            loadingIndicator.stopAnimating()
            refreshControl.endRefreshing()
        }
    }
    
    func successGetMovieGenre() {
        DispatchQueue.main.async { [self] in
            tableView.reloadData()
        }
    }
    
    func failGetMovieGenre(error: String) {
        DialogHelper.showAlert(title: "Error", description: error, viewController: self)
    }
}

extension GenreListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueCell(withType: GenreTableViewCell.self, for: indexPath) as! GenreTableViewCell
        cell.setGenre(text: presenter?.genreEntity[safe: indexPath.row]?.name ?? "")
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter?.genreEntity.count ?? 0
    }
    
}

extension GenreListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let data = presenter?.genreEntity[indexPath.row]
        presenter?.moveToMovieListByGenre(id: data?.id ?? 0, title: data?.name ?? "")
    }
}
