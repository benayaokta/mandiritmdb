//
//  GenreListContract.swift
//  MandiriTMDB
//
//  Created by Benaya Oktavianus on 04/02/24.
//  
//

import Foundation
import UIKit.UIViewController

// MARK: View Output (Presenter -> View)
protocol PresenterToViewGenreListProtocol {
    func successGetMovieGenre()
    func failGetMovieGenre(error: String)
    func showLoading()
    func hideLoading()
}


// MARK: View Input (View -> Presenter)
protocol ViewToPresenterGenreListProtocol {
    
    var view: PresenterToViewGenreListProtocol? { get set }
    var interactor: PresenterToInteractorGenreListProtocol? { get set }
    var router: PresenterToRouterGenreListProtocol? { get set }
    var genreEntity: [GenreEntity] { get }
    
    func getMovieGenre()
    
    func moveToMovieListByGenre(id: Int, title: String)
}


// MARK: Interactor Input (Presenter -> Interactor)
protocol PresenterToInteractorGenreListProtocol {
    
    var presenter: InteractorToPresenterGenreListProtocol? { get set }
    var genreEntity: [GenreEntity] { get set }
    func getMovieGenre()
}


// MARK: Interactor Output (Interactor -> Presenter)
protocol InteractorToPresenterGenreListProtocol {
    func successGetGenre()
    func failGetGenre(error: String)
}


// MARK: Router Input (Presenter -> Router)
protocol PresenterToRouterGenreListProtocol {
    var routerView: UIViewController? { get set }
    
    func moveToMovieList(id: Int, title: String)
}
