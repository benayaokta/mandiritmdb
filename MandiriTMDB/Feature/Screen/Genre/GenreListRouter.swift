//
//  GenreListRouter.swift
//  MandiriTMDB
//
//  Created by Benaya Oktavianus on 04/02/24.
//  
//

import Foundation
import UIKit

class GenreListRouter: PresenterToRouterGenreListProtocol {
    weak var routerView: UIViewController?
    // MARK: Static methods
    static func createModule() -> UIViewController {
        
        let viewController = GenreListViewController()
        
        let presenter: ViewToPresenterGenreListProtocol & InteractorToPresenterGenreListProtocol = GenreListPresenter()
        
        viewController.presenter = presenter
        viewController.presenter?.router = GenreListRouter()
        viewController.presenter?.router?.routerView = viewController
        viewController.presenter?.view = viewController
        viewController.presenter?.interactor = GenreListInteractor(repo: CommonRepoImplementation())
        viewController.presenter?.interactor?.presenter = presenter
        
        return viewController
    }
    
    func moveToMovieList(id: Int, title: String) {
        let genreMovieList = GenreMovieListRouter.createModule(genre: id, title: title)
        routerView?.navigationController?.pushViewController(genreMovieList, animated: true)
    }
    
}
