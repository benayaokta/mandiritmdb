//
//  MovieDetailPresenter.swift
//  MandiriTMDB
//
//  Created by Benaya Oktavianus on 03/02/24.
//  
//

import Foundation

class MovieDetailPresenter: ViewToPresenterMovieDetailProtocol {

    // MARK: Properties
    var view: PresenterToViewMovieDetailProtocol?
    var interactor: PresenterToInteractorMovieDetailProtocol?
    var router: PresenterToRouterMovieDetailProtocol?
    
    var movieEntity: MovieDetailEntity {
        return interactor?.movieDetailEntity ?? MovieDetailEntity()
    }
    
    var movieVideoEntity: MovieVideoEntity {
        return interactor?.movieVideoEntity ?? MovieVideoEntity()
    }
    
    func getMovieDetail() {
        view?.showLoading()
        interactor?.getMovieDetail()
    }
    
    func getMovieVideoDetail() {
        interactor?.getMovieVideoDetail()
    }
}

extension MovieDetailPresenter: InteractorToPresenterMovieDetailProtocol {
    func successGetMovieDetail() {
        view?.hideLoading()
        view?.successGetMovieEntity()
    }
    
    func failGetMovieDetail(error: String) {
        view?.hideLoading()
        view?.failGetMovieEntity(error: error)
    }
    
    func goToMovieReview() {
        router?.goToReviewView(id: movieEntity.id ?? 0)
    }
    
    func successGetMovieVideoDetail() {
        view?.successGetMovieVideoDetail()
    }
    
    func failGetMovieVideoDetail(error: String) {
        view?.failGetMovieVideoDetail(error: error)
    }
}
