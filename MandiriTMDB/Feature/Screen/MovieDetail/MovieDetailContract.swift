//
//  MovieDetailContract.swift
//  MandiriTMDB
//
//  Created by Benaya Oktavianus on 03/02/24.
//  
//

import Foundation
import UIKit.UIViewController

// MARK: View Output (Presenter -> View)
protocol PresenterToViewMovieDetailProtocol {
    func showLoading()
    func hideLoading()
    func successGetMovieEntity()
    func failGetMovieEntity(error: String)
    func failGetMovieVideoDetail(error: String)
    func successGetMovieVideoDetail()
}

// MARK: View Input (View -> Presenter)
protocol ViewToPresenterMovieDetailProtocol {
    
    var view: PresenterToViewMovieDetailProtocol? { get set }
    var interactor: PresenterToInteractorMovieDetailProtocol? { get set }
    var router: PresenterToRouterMovieDetailProtocol? { get set }
    var movieEntity: MovieDetailEntity { get }
    var movieVideoEntity: MovieVideoEntity { get }
    func getMovieDetail()
    func getMovieVideoDetail()
    func goToMovieReview()
}


// MARK: Interactor Input (Presenter -> Interactor)
protocol PresenterToInteractorMovieDetailProtocol {
    
    var presenter: InteractorToPresenterMovieDetailProtocol? { get set }
    var movieDetailEntity: MovieDetailEntity { get set }
    var movieVideoEntity: MovieVideoEntity { get set }
    func getMovieDetail()
    func getMovieVideoDetail()
}


// MARK: Interactor Output (Interactor -> Presenter)
protocol InteractorToPresenterMovieDetailProtocol {
    func successGetMovieDetail()
    func failGetMovieDetail(error: String)
    
    func successGetMovieVideoDetail()
    func failGetMovieVideoDetail(error: String)
}

// MARK: Router Input (Presenter -> Router)
protocol PresenterToRouterMovieDetailProtocol {
    func goToReviewView(id: Int)
    var routerView: UIViewController? { get set }
}
