//
//  MovieDetailViewController.swift
//  MandiriTMDB
//
//  Created by Benaya Oktavianus on 03/02/24.
//  
//

import UIKit
import Stevia
import SDWebImage

class MovieDetailViewController: BaseViewController {
    
    var presenter: ViewToPresenterMovieDetailProtocol?
    
    private let indicator: UIActivityIndicatorView = UIActivityIndicatorView(frame: .zero)
    private let scrollView: UIScrollView = UIScrollView()
    private let contentWrapper: UIView = UIView()
    private let movieImage: UIImageView = UIImageView()
    private let movieName: UILabel = UILabel()
    private let movieOverview: UILabel = UILabel()
    private let seeHomePage: UIButton = UIButton()
    private let seeReviews: UIButton = UIButton()
    private let releaseDate: UILabel = UILabel()
    private let seeTrailer: UIButton = UIButton()
    
    private var canTapButton: Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()
        setupHierarchy()
        setupConstraint()
        setupStyle()
        setupActions()
        presenter?.getMovieDetail()
        presenter?.getMovieVideoDetail()
    }
    
    private func setupHierarchy() {
        self.view.subviews {
            scrollView.subviews {
                contentWrapper.subviews {
                    movieImage
                    releaseDate
                    movieName
                    movieOverview
                    seeTrailer
                    seeHomePage
                    seeReviews
                    indicator
                }
            }
            
        }
    }
    
    private func setupConstraint() {
        scrollView.Leading == self.view.Leading
        scrollView.Trailing == self.view.Trailing
        scrollView.Top == self.view.Top
        scrollView.Bottom == self.view.Bottom
        
        contentWrapper.fillContainer().Width == scrollView.Width
        
        movieImage.fillHorizontally(padding: 16).Top == contentWrapper.Top + 16
        movieImage.Height == movieImage.Width * 1.5
        
        movieName.Leading == contentWrapper.Leading + 16
        movieName.Top == movieImage.Bottom + 16
        movieName.Trailing == contentWrapper.Trailing - 16
        
        releaseDate.Leading == movieName.Leading
        releaseDate.Trailing == movieName.Trailing
        releaseDate.Top == movieName.Bottom
        
        movieOverview.fillHorizontally(padding: 16).Top == releaseDate.Bottom + 16
        
        seeTrailer.height(0).fillHorizontally(padding: 16).Top == movieOverview.Bottom + 16
        seeTrailer.backgroundColor = .white
        
        seeHomePage.height(0).fillHorizontally(padding: 16).Top == seeTrailer.Bottom + 16
        seeHomePage.backgroundColor = UIColor.white
        
        seeReviews.height(50).fillHorizontally(padding: 16).Top == seeHomePage.Bottom + 16
        seeReviews.Bottom == contentWrapper.Bottom - 16
        seeReviews.backgroundColor = UIColor.systemBlue
        seeReviews.layer.cornerRadius = 10
        
        indicator.centerInContainer().width(40).heightEqualsWidth()
    }
    
    private func setupStyle() {
        self.title = "Detail"
        indicator.hidesWhenStopped = true

        movieName.font = UIFont.systemFont(ofSize: 18, weight: .semibold)
        movieOverview.numberOfLines = 0
        seeTrailer.isHidden = true
        seeTrailer.setTitle("See Movie Trailer", for: .normal)
        seeTrailer.setTitleColor(UIColor.systemBlue, for: .normal)
        
        seeHomePage.setTitle("See Movie Homepage", for: .normal)
        seeHomePage.setTitleColor(UIColor.systemBlue, for: .normal)
        
        seeReviews.setTitle("See Movie Reviews", for: .normal)
    }
    
    private func setupActions() {
        seeHomePage.addTarget(self, action: #selector(seeMovieHomePageURL), for: .touchUpInside)
        seeTrailer.addTarget(self, action: #selector(seeTrailerAction), for: .touchUpInside)
        seeReviews.addTarget(self, action: #selector(seeReviewsAction), for: .touchUpInside)
    }
    
    @objc private func seeMovieHomePageURL() {
        if canTapButton {
            let data = presenter?.movieEntity
            if let url = URL(string: data?.homepage ?? "") {
                if UIApplication.shared.canOpenURL(url) {
                    UIApplication.shared.open(url)
                }
            }
        }
    }
    
    @objc private func seeReviewsAction() {
        presenter?.goToMovieReview()
    }
    
    @objc private func seeTrailerAction() {
        let data = presenter?.movieVideoEntity
        if let youtube = data?.results?.first(where: {$0.site == .youTube && $0.type == .trailer}) {
            let combineURL = "https://www.youtube.com/watch?v=\(youtube.key ?? "")"
            if let url = URL(string: combineURL) {
                if UIApplication.shared.canOpenURL(url) {
                    UIApplication.shared.open(url)
                }
            }
        }
    }
}

extension MovieDetailViewController: PresenterToViewMovieDetailProtocol{
    func showLoading() {
        DispatchQueue.main.async { [self] in
            indicator.startAnimating()
        }
    }
    
    func hideLoading() {
        DispatchQueue.main.async { [self] in
            indicator.stopAnimating()
        }
    }
    
    func successGetMovieEntity() {
        let data = presenter?.movieEntity
        canTapButton = true
        DispatchQueue.main.async {
            if data?.homepage == "" {
                self.seeHomePage.isHidden = true
            } else {
                self.seeHomePage.height(50)
            }
            let urlString = "https://image.tmdb.org/t/p/w500\(data?.posterPath ?? "")"
            self.movieImage.sd_setImage(with: URL(string: urlString), placeholderImage: UIImage(systemName: "photo"), options: [.progressiveLoad])
            self.movieImage.sd_imageIndicator = SDWebImageProgressIndicator()
            self.movieName.text = data?.originalTitle ?? ""
            self.releaseDate.text = data?.releaseDate ?? ""
            self.movieOverview.text = data?.overview ?? ""
        }
    }
    
    func failGetMovieEntity(error: String) {
        DialogHelper.showAlert(title: "Error getting movie detail", description: error, viewController: self)
    }
    
    func successGetMovieVideoDetail() {
        DispatchQueue.main.async { [self] in
            seeTrailer.height(50)
            seeTrailer.isHidden = false
        }
    }
    
    func failGetMovieVideoDetail(error: String) {
        // do nothing for now
    }
    
}
