//
//  MovieDetailRouter.swift
//  MandiriTMDB
//
//  Created by Benaya Oktavianus on 03/02/24.
//  
//

import Foundation
import UIKit

class MovieDetailRouter: PresenterToRouterMovieDetailProtocol {
    
    weak var routerView: UIViewController?
    
    // MARK: Static methods
    static func createModule(id: Int) -> UIViewController {
        
        let viewController = MovieDetailViewController()
        
        let presenter: ViewToPresenterMovieDetailProtocol & InteractorToPresenterMovieDetailProtocol = MovieDetailPresenter()
        
        viewController.presenter = presenter
        viewController.presenter?.router = MovieDetailRouter()
        viewController.presenter?.router?.routerView = viewController
        viewController.presenter?.view = viewController
        viewController.presenter?.interactor = MovieDetailInteractor(movieID: id, repo: CommonRepoImplementation())
        viewController.presenter?.interactor?.presenter = presenter
        
        return viewController
    }
    
    func goToReviewView(id: Int) {
        let review = ReviewRouter.createModule(id: id)
        routerView?.navigationController?.pushViewController(review, animated: true)
    }
}
