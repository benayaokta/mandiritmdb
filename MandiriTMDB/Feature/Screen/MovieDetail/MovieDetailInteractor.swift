//
//  MovieDetailInteractor.swift
//  MandiriTMDB
//
//  Created by Benaya Oktavianus on 03/02/24.
//  
//

import Foundation

class MovieDetailInteractor: PresenterToInteractorMovieDetailProtocol {

    // MARK: Properties
    var presenter: InteractorToPresenterMovieDetailProtocol?
    
    let movieID: Int
    let repo: CommonRepo
    
    var movieDetailEntity: MovieDetailEntity = MovieDetailEntity()
    var movieVideoEntity: MovieVideoEntity = MovieVideoEntity()
    
    init(movieID: Int, repo: CommonRepo) {
        self.movieID = movieID
        self.repo = repo
    }
    
    func getMovieDetail() {
        repo.getMovieDetail(id: self.movieID) { [weak self] result in
            switch result {
            case .success(let success):
                self?.movieDetailEntity = success.toDomain()
                self?.presenter?.successGetMovieDetail()
            case .failure(let failure):
                self?.presenter?.failGetMovieDetail(error: failure.localizedDescription)
            }
        }
    }
    
    func getMovieVideoDetail() {
        repo.getMovieVideo(id: self.movieID) { [weak self] result in
            switch result {
            case .success(let success):
                self?.movieVideoEntity = success.toDomain()
                self?.presenter?.successGetMovieVideoDetail()
            case .failure(let failure):
                self?.presenter?.failGetMovieVideoDetail(error: failure.localizedDescription)
            }
        }
    }
}
