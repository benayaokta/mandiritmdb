//
//  ReviewRouter.swift
//  MandiriTMDB
//
//  Created by Benaya Oktavianus on 04/02/24.
//  
//

import Foundation
import UIKit

class ReviewRouter: PresenterToRouterReviewProtocol {
    
    weak var routerView: UIViewController?
    
    // MARK: Static methods
    static func createModule(id: Int) -> UIViewController {
        
        let viewController = ReviewViewController()
        
        let presenter: ViewToPresenterReviewProtocol & InteractorToPresenterReviewProtocol = ReviewPresenter()
        
        viewController.presenter = presenter
        viewController.presenter?.router = ReviewRouter()
        viewController.presenter?.router?.routerView = viewController
        viewController.presenter?.view = viewController
        viewController.presenter?.interactor = ReviewInteractor(id: id, repo: CommonRepoImplementation())
        viewController.presenter?.interactor?.presenter = presenter
        
        return viewController
    }
    
}
