//
//  ReviewPresenter.swift
//  MandiriTMDB
//
//  Created by Benaya Oktavianus on 04/02/24.
//  
//

import Foundation

class ReviewPresenter: ViewToPresenterReviewProtocol {

    // MARK: Properties
    var view: PresenterToViewReviewProtocol?
    var interactor: PresenterToInteractorReviewProtocol?
    var router: PresenterToRouterReviewProtocol?
    
    var reviewEntity: [ReviewResultEntity] {
        return interactor?.reviewEntity ?? []
    }
    
    func getReview() {
        interactor?.getReview()
    }
}

extension ReviewPresenter: InteractorToPresenterReviewProtocol {
    func successGetReview() {
        view?.hideLoading()
        view?.successGetReview()
    }
    
    func failGetReview(error: String) {
        view?.hideLoading()
        view?.failGetReview(error: error)
    }

}
