//
//  ReviewContract.swift
//  MandiriTMDB
//
//  Created by Benaya Oktavianus on 04/02/24.
//  
//

import Foundation
import UIKit.UIViewController

// MARK: View Output (Presenter -> View)
protocol PresenterToViewReviewProtocol {
    func showLoading()
    func hideLoading()
    func successGetReview()
    func failGetReview(error: String)
}


// MARK: View Input (View -> Presenter)
protocol ViewToPresenterReviewProtocol {
    
    var view: PresenterToViewReviewProtocol? { get set }
    var interactor: PresenterToInteractorReviewProtocol? { get set }
    var router: PresenterToRouterReviewProtocol? { get set }
    
    var reviewEntity: [ReviewResultEntity] { get }
    
    func getReview()
}


// MARK: Interactor Input (Presenter -> Interactor)
protocol PresenterToInteractorReviewProtocol {
    
    var presenter: InteractorToPresenterReviewProtocol? { get set }
    
    var reviewEntity: [ReviewResultEntity] { get set }
    
    func getReview()
}


// MARK: Interactor Output (Interactor -> Presenter)
protocol InteractorToPresenterReviewProtocol {
    func successGetReview()
    func failGetReview(error: String)
}


// MARK: Router Input (Presenter -> Router)
protocol PresenterToRouterReviewProtocol {
    var routerView: UIViewController? { get set }
}
