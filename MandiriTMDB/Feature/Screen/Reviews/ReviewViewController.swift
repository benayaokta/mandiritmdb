//
//  ReviewViewController.swift
//  MandiriTMDB
//
//  Created by Benaya Oktavianus on 04/02/24.
//  
//

import UIKit
import Stevia

class ReviewViewController: BaseViewController {
    
    var presenter: ViewToPresenterReviewProtocol?
    
    private let tableView: UITableView = UITableView()
    private let activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView(frame: .zero)
    private let emptyView: UIView = UIView()
    private let emptyLabel: UILabel = UILabel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupHierarchy()
        setupConstraint()
        setupStyle()
        setupTableView()
        setupActions()
        presenter?.getReview()
    }
    
    private func setupHierarchy() {
        self.view.subviews {
            tableView
            emptyView.subviews {
                emptyLabel
            }
            activityIndicator
        }
    }
    
    private func setupConstraint() {
        tableView.fillContainer()
        emptyView.fillContainer()
        emptyLabel.centerInContainer()
        activityIndicator.centerInContainer().width(40).heightEqualsWidth()
    }
    
    private func setupStyle() {
        self.title = "Review"
        activityIndicator.hidesWhenStopped = true
        emptyView.isHidden = true
        emptyLabel.text = "No Reviews"
    }
    
    private func setupActions() {
       
    }
    
    private func setupTableView() {
        tableView.dataSource = self
        tableView.delegate = self
        tableView.registerCellClass(type: ReviewTableViewCell.self)
    }

}

extension ReviewViewController: PresenterToViewReviewProtocol{
    func showLoading() {
        DispatchQueue.main.async {
            self.activityIndicator.startAnimating()
        }
    }
    
    func hideLoading() {
        DispatchQueue.main.async {
            self.activityIndicator.stopAnimating()
        }
    }
    
    func successGetReview() {
        DispatchQueue.main.async {
            if self.presenter?.reviewEntity.count == 0 {
                self.tableView.isHidden = true
                self.emptyView.isHidden = false
            } else {
                self.tableView.isHidden = false
                self.tableView.reloadData()
            }  
        }
    }
    
    func failGetReview(error: String) {
        DialogHelper.showAlert(title: "Error", description: error, viewController: self)
    }
}

extension ReviewViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueCell(withType: ReviewTableViewCell.self, for: indexPath) as! ReviewTableViewCell
        let data = presenter?.reviewEntity[safe: indexPath.row]
        cell.setImageAuthor(url: data?.authorDetails?.avatarPath ?? "")
        cell.setAuthorName(text: data?.authorDetails?.username ?? "")
        cell.setAuthorComment(text: data?.content ?? "")
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter?.reviewEntity.count ?? 0
    }
}

extension ReviewViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
