//
//  ReviewInteractor.swift
//  MandiriTMDB
//
//  Created by Benaya Oktavianus on 04/02/24.
//  
//

import Foundation

class ReviewInteractor: PresenterToInteractorReviewProtocol {

    // MARK: Properties
    var presenter: InteractorToPresenterReviewProtocol?
    
    var reviewEntity: [ReviewResultEntity] = []
    
    let repo: CommonRepo
    let movieID: Int
    init(id: Int, repo: CommonRepo) {
        self.movieID = id
        self.repo = repo
    }
    
    func getReview() {
        repo.getMovieReviews(id: self.movieID) { [weak self] result in
            switch result {
            case .success(let success):
                let domain = success.results?.map({$0.toDomain()})
                self?.reviewEntity.append(contentsOf: domain ?? [])
                self?.presenter?.successGetReview()
            case .failure(let failure):
                self?.presenter?.failGetReview(error: failure.localizedDescription)
            }
        }
    }
}
