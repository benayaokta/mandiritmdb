//
//  GenreMovieListViewController.swift
//  MandiriTMDB
//
//  Created by Benaya Oktavianus on 04/02/24.
//  
//

import UIKit
import Stevia

class GenreMovieListViewController: BaseViewController {
    
    var presenter: ViewToPresenterGenreMovieListProtocol?
    
    private let genreMovieListTableView: UITableView = UITableView()
    private let loadingIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    
    private var isLoading: Bool = false
    private var page: Int = 1
    override func viewDidLoad() {
        super.viewDidLoad()
        setupHierarchy()
        setupConstraint()
        setupTableView()
        setupStyle()
        setupActions()
        presenter?.getMovieListFromGenreID(page: page, infinite: false)
    }
    
    private func setupHierarchy() {
        self.view.subviews {
            genreMovieListTableView
            loadingIndicator
        }
    }
    
    private func setupConstraint() {
        genreMovieListTableView.fillHorizontally().Top == self.view.safeAreaLayoutGuide.Top
        genreMovieListTableView.Bottom == self.view.safeAreaLayoutGuide.Bottom
        
        loadingIndicator.centerInContainer().width(40).heightEqualsWidth()
    }
    
    private func setupStyle() {
        self.title = presenter?.title
        loadingIndicator.tintColor = .black
        loadingIndicator.hidesWhenStopped = true
    }
    
    private func setupActions() {
        
    }
    
    private func setupTableView() {
        genreMovieListTableView.backgroundColor = .white
        genreMovieListTableView.delegate = self
        genreMovieListTableView.dataSource = self
        genreMovieListTableView.registerCellClass(type: HomeTableViewCell.self)
        genreMovieListTableView.registerCellClass(type: HomeLoadingCell.self)
    }
}

extension GenreMovieListViewController: PresenterToViewGenreMovieListProtocol{
    func successGetMovieList() {
        reloadTable()
    }
    
    func failGetMovieList(error: String) {
        DialogHelper.showAlert(title: "Error", description: error, viewController: self)
    }
    
    func showLoading() {
        DispatchQueue.main.async { [self] in
            loadingIndicator.startAnimating()
        }
    }
    
    func hideLoading() {
        DispatchQueue.main.async { [self] in
            loadingIndicator.stopAnimating()
        }
    }
    
    func toggleIsLoading(bool: Bool) {
        self.isLoading = false
    }
    
    private func reloadTable() {
        DispatchQueue.main.async { [self] in
            genreMovieListTableView.reloadData()
        }
    }
}

extension GenreMovieListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let data = presenter?.resultEntity[safe: indexPath.row]
        presenter?.goToMovieDetail(id: data?.id ?? 0)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastRowIndex = tableView.numberOfRows(inSection: 0) - 1
        if indexPath.row == lastRowIndex && !isLoading {
            // Load more data when the last cell is about to be displayed
            isLoading = true
            self.reloadTable()
            page += 1
            presenter?.getMovieListFromGenreID(page: page, infinite: true)
        }
    }
}

extension GenreMovieListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return isLoading ? (presenter?.resultEntity.count ?? 0) + 1 : presenter?.resultEntity.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if isLoading {
            let cell = tableView.dequeueCell(withType: HomeLoadingCell.self, for: indexPath) as! HomeLoadingCell
            return cell
        } else {
            let cell = tableView.dequeueCell(withType: HomeTableViewCell.self, for: indexPath) as! HomeTableViewCell
            let data = presenter?.resultEntity[safe: indexPath.row]
            cell.setImage(url: data?.posterPath ?? "")
            cell.setMovie(name: data?.originalTitle ?? "")
            cell.setMovie(desc: data?.overview ?? "")
            return cell
        }
    }
}
