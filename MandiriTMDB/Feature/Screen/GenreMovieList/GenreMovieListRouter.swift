//
//  GenreMovieListRouter.swift
//  MandiriTMDB
//
//  Created by Benaya Oktavianus on 04/02/24.
//  
//

import Foundation
import UIKit

class GenreMovieListRouter: PresenterToRouterGenreMovieListProtocol {
    
    weak var routerView: UIViewController?
    
    // MARK: Static methods
    static func createModule(genre id: Int, title: String) -> UIViewController {
        
        let viewController = GenreMovieListViewController()
        
        let presenter: ViewToPresenterGenreMovieListProtocol & InteractorToPresenterGenreMovieListProtocol = GenreMovieListPresenter()
        
        viewController.presenter = presenter
        viewController.presenter?.router = GenreMovieListRouter()
        viewController.presenter?.router?.routerView = viewController
        viewController.presenter?.view = viewController
        viewController.presenter?.interactor = GenreMovieListInteractor(genreID: id, title: title, repo: CommonRepoImplementation())
        viewController.presenter?.interactor?.presenter = presenter
        
        return viewController
    }
    
    func goToMovieDetail(id: Int) {
        let detail = MovieDetailRouter.createModule(id: id)
        self.routerView?.navigationController?.pushViewController(detail, animated: true)
    }
}
