//
//  GenreMovieListContract.swift
//  MandiriTMDB
//
//  Created by Benaya Oktavianus on 04/02/24.
//  
//

import Foundation
import UIKit.UIViewController

// MARK: View Output (Presenter -> View)
protocol PresenterToViewGenreMovieListProtocol {
    func successGetMovieList()
    func failGetMovieList(error: String)
    func showLoading()
    func hideLoading()
    func toggleIsLoading(bool: Bool)
}

// MARK: View Input (View -> Presenter)
protocol ViewToPresenterGenreMovieListProtocol {
    
    var view: PresenterToViewGenreMovieListProtocol? { get set }
    var interactor: PresenterToInteractorGenreMovieListProtocol? { get set }
    var router: PresenterToRouterGenreMovieListProtocol? { get set }
    
    var resultEntity: [ResultEntity] { get }
    var title: String { get }
    
    func getMovieListFromGenreID(page: Int, infinite: Bool)
    func goToMovieDetail(id: Int)
    
}


// MARK: Interactor Input (Presenter -> Interactor)
protocol PresenterToInteractorGenreMovieListProtocol {
    
    var presenter: InteractorToPresenterGenreMovieListProtocol? { get set }
    var movieList: [ResultEntity] { get set }
    var genreTitle: String { get set }
    func getMovieByGenre(page: Int)
}


// MARK: Interactor Output (Interactor -> Presenter)
protocol InteractorToPresenterGenreMovieListProtocol {
    func successGetMovieByGenreID()
    func failGetMovieByGenreID(error: String)
}


// MARK: Router Input (Presenter -> Router)
protocol PresenterToRouterGenreMovieListProtocol {
    var routerView: UIViewController? { get set }
    func goToMovieDetail(id: Int)
}
