//
//  GenreMovieListInteractor.swift
//  MandiriTMDB
//
//  Created by Benaya Oktavianus on 04/02/24.
//  
//

import Foundation

class GenreMovieListInteractor: PresenterToInteractorGenreMovieListProtocol {

    // MARK: Properties
    var presenter: InteractorToPresenterGenreMovieListProtocol?
    
    let genreID: Int
    var genreTitle: String
    let repo: CommonRepo
    
    var movieList: [ResultEntity] = []
    
    init(genreID: Int, title: String, repo: CommonRepo) {
        self.genreTitle = title
        self.genreID = genreID
        self.repo = repo
    }
    
    func getMovieByGenre(page: Int) {
        repo.getMovieFromGenre(id: genreID, page: page) { [weak self] response in
            switch response {
            case .success(let success):
                self?.movieList.append(contentsOf: DiscoverEntity.mapFromDiscoverData(response: success).results)
                self?.presenter?.successGetMovieByGenreID()
            case .failure(let failure):
                self?.presenter?.failGetMovieByGenreID(error: failure.localizedDescription)
            }
        }
    }
    
    
}
