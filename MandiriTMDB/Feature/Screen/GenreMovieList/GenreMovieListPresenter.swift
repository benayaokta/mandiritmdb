//
//  GenreMovieListPresenter.swift
//  MandiriTMDB
//
//  Created by Benaya Oktavianus on 04/02/24.
//  
//

import Foundation

class GenreMovieListPresenter: ViewToPresenterGenreMovieListProtocol {

    // MARK: Properties
    var view: PresenterToViewGenreMovieListProtocol?
    var interactor: PresenterToInteractorGenreMovieListProtocol?
    var router: PresenterToRouterGenreMovieListProtocol?
    
    private var fromInifiniteScroll: Bool = false
    
    var title: String {
        return interactor?.genreTitle ?? ""
    }
    
    var resultEntity: [ResultEntity] {
        return interactor?.movieList ?? []
    }
    
    func getMovieListFromGenreID(page: Int, infinite: Bool) {
        fromInifiniteScroll = infinite
        view?.showLoading()
        interactor?.getMovieByGenre(page: page)
    }
    
    func goToMovieDetail(id: Int) {
        router?.goToMovieDetail(id: id)
    }
}

extension GenreMovieListPresenter: InteractorToPresenterGenreMovieListProtocol {
    func successGetMovieByGenreID() {
        
        if fromInifiniteScroll {
            view?.toggleIsLoading(bool: false)
            fromInifiniteScroll = false
        }
        view?.hideLoading()
        view?.successGetMovieList()
    }
    
    func failGetMovieByGenreID(error: String) {
        if fromInifiniteScroll {
            view?.toggleIsLoading(bool: false)
            fromInifiniteScroll = false
        }
        view?.hideLoading()
        view?.failGetMovieList(error: error)
    }

}
