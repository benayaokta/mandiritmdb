//
//  MovieVideoEntity.swift
//  MandiriTMDB
//
//  Created by Benaya Oktavianus on 04/02/24.
//

import Foundation

// MARK: - MovieVideoEntity
struct MovieVideoEntity {
    let id: Int?
    let results: [VideoResultEntity]?
    
    init() {
        self.id = 0
        self.results = []
    }
    
    init(id: Int?, results: [VideoResultEntity]?) {
        self.id = id
        self.results = results
    }
}

// MARK: - VideoResultEntity
struct VideoResultEntity {
    let iso639_1: String?
    let iso3166_1: String?
    let name, key: String?
    let site: SiteEnum?
    let size: Int?
    let type: VideoTypeEnum?
    let official: Bool?
    let publishedAt, id: String?
    
    init(iso639_1: String?, iso3166_1: String?, name: String?, key: String?, site: SiteEnum?, size: Int?, type: VideoTypeEnum?, official: Bool?, publishedAt: String?, id: String?) {
        self.iso639_1 = iso639_1
        self.iso3166_1 = iso3166_1
        self.name = name
        self.key = key
        self.site = site
        self.size = size
        self.type = type
        self.official = official
        self.publishedAt = publishedAt
        self.id = id
    }
    
    init() {
        self.iso639_1 = nil
        self.iso3166_1 = nil
        self.name = nil
        self.key = nil
        self.site = nil
        self.size = nil
        self.type = nil
        self.official = nil
        self.publishedAt = nil
        self.id = nil
    }
}

// You can create an extension for each data layer struct to convert it into its corresponding domain layer struct.
extension MovieVideoResponseModel {
    func toDomain() -> MovieVideoEntity {
        return MovieVideoEntity(
            id: id,
            results: results?.map { $0.toDomain() }
        )
    }
}

extension VideoResult {
    func toDomain() -> VideoResultEntity {
        return VideoResultEntity(
            iso639_1: iso639_1,
            iso3166_1: iso3166_1,
            name: name,
            key: key,
            site: SiteEnum(rawValue: site ?? ""),
            size: size,
            type: VideoTypeEnum(rawValue: type ?? ""),
            official: official,
            publishedAt: publishedAt,
            id: id
        )
    }
}


enum SiteEnum: String {
    case youTube = "YouTube"
    
    init?(rawValue: String) {
        switch rawValue {
        case "YouTube": self = .youTube
        default: self = .youTube
        }
    }
}

enum VideoTypeEnum: String {
    case clip = "Clip"
    case featurette = "Featurette"
    case teaser = "Teaser"
    case behindTheScenes = "Behind the Scenes"
    case trailer = "Trailer"
    
    init?(rawValue: String) {
        switch rawValue {
        case "Clip":
            self = .clip
        case "Featurette":
            self = .featurette
        case "Teaser":
            self = .teaser
        case "Behind the Scenes":
            self = .behindTheScenes
        case "Trailer":
            self = .trailer
        default:
            return nil
        }
    }
}
