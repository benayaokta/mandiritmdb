//
//  ReviewEntity.swift
//  MandiriTMDB
//
//  Created by Benaya Oktavianus on 04/02/24.
//

import Foundation

// MARK: - ReviewEntity
struct ReviewEntity {
    let id, page: Int?
    let results: [ReviewResultEntity]?
    let totalPages, totalResults: Int?
}

// MARK: - ReviewResult
struct ReviewResultEntity {
    let author: String?
    let authorDetails: ReviewAuthorDetails?
    let content, createdAt, id, updatedAt: String?
    let url: String?
}

// MARK: - ReviewAuthorDetails
struct ReviewAuthorDetails {
    let name, username: String?
    let avatarPath: String?
    let rating: Double?
}

// You can create an extension for each data layer struct to convert it into its corresponding domain layer struct.
extension ReviewResponseModel {
    func toDomain() -> ReviewEntity {
        return ReviewEntity(
            id: id,
            page: page,
            results: results?.map { $0.toDomain() },
            totalPages: totalPages,
            totalResults: totalResults
        )
    }
}

extension ReviewItem {
    func toDomain() -> ReviewResultEntity {
        return ReviewResultEntity(
            author: author,
            authorDetails: authorDetails?.toDomain(),
            content: content,
            createdAt: createdAt,
            id: id,
            updatedAt: updatedAt,
            url: url
        )
    }
}

extension AuthorDetails {
    func toDomain() -> ReviewAuthorDetails {
        return ReviewAuthorDetails(
            name: name,
            username: username,
            avatarPath: avatarPath,
            rating: rating
        )
    }
}
