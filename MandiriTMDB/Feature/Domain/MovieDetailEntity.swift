//
//  MovieDetailEntity.swift
//  MandiriTMDB
//
//  Created by Benaya Oktavianus on 03/02/24.
//

import Foundation

// MARK: - MovieDetail
struct MovieDetailEntity {
    let adult: Bool?
    let backdropPath: String?
    let budget: Int?
    let genres: [MovieGenre]?
    let homepage: String?
    let id: Int?
    let imdbID, originalLanguage, originalTitle, overview: String?
    let popularity: Double?
    let posterPath: String?
    let productionCompanies: [MovieProductionCompany]?
    let productionCountries: [MovieProductionCountry]?
    let releaseDate: String?
    let revenue, runtime: Int?
    let spokenLanguages: [MovieSpokenLanguage]?
    let status, tagline, title: String?
    let video: Bool?
    let voteAverage: Double?
    let voteCount: Int?
    
    init(adult: Bool?, backdropPath: String?, budget: Int?, genres: [MovieGenre]?, homepage: String?, id: Int?, imdbID: String?, originalLanguage: String?, originalTitle: String?, overview: String?, popularity: Double?, posterPath: String?, productionCompanies: [MovieProductionCompany]?, productionCountries: [MovieProductionCountry]?, releaseDate: String?, revenue: Int?, runtime: Int?, spokenLanguages: [MovieSpokenLanguage]?, status: String?, tagline: String?, title: String?, video: Bool?, voteAverage: Double?, voteCount: Int?) {
        self.adult = adult
        self.backdropPath = backdropPath
        self.budget = budget
        self.genres = genres
        self.homepage = homepage
        self.id = id
        self.imdbID = imdbID
        self.originalLanguage = originalLanguage
        self.originalTitle = originalTitle
        self.overview = overview
        self.popularity = popularity
        self.posterPath = posterPath
        self.productionCompanies = productionCompanies
        self.productionCountries = productionCountries
        self.releaseDate = releaseDate
        self.revenue = revenue
        self.runtime = runtime
        self.spokenLanguages = spokenLanguages
        self.status = status
        self.tagline = tagline
        self.title = title
        self.video = video
        self.voteAverage = voteAverage
        self.voteCount = voteCount
    }
    
    init() {
        self.adult = nil
        self.backdropPath = nil
        self.budget = nil
        self.genres = nil
        self.homepage = nil
        self.id = nil
        self.imdbID = nil
        self.originalLanguage = nil
        self.originalTitle = nil
        self.overview = nil
        self.popularity = nil
        self.posterPath = nil
        self.productionCompanies = nil
        self.productionCountries = nil
        self.releaseDate = nil
        self.revenue = nil
        self.runtime = nil
        self.spokenLanguages = nil
        self.status = nil
        self.tagline = nil
        self.title = nil
        self.video = nil
        self.voteAverage = nil
        self.voteCount = nil
    }
}

// MARK: - MovieGenre
struct MovieGenre {
    let id: Int?
    let name: String?
}

// MARK: - MovieProductionCompany
struct MovieProductionCompany {
    let id: Int?
    let logoPath: String?
    let name, originCountry: String?
}

// MARK: - MovieProductionCountry
struct MovieProductionCountry {
    let iso3166_1, name: String?
}

// MARK: - MovieSpokenLanguage
struct MovieSpokenLanguage {
    let englishName, iso639_1, name: String?
}

// You can also create an extension for each data layer struct to convert it into its corresponding domain layer struct.
extension MovieDetailResponseModel {
    func toDomain() -> MovieDetailEntity {
        return MovieDetailEntity(
            adult: adult,
            backdropPath: backdropPath,
            budget: budget,
            genres: genres?.map { $0.toDomain() },
            homepage: homepage,
            id: id,
            imdbID: imdbID,
            originalLanguage: originalLanguage,
            originalTitle: originalTitle,
            overview: overview,
            popularity: popularity,
            posterPath: posterPath,
            productionCompanies: productionCompanies?.map { $0.toDomain() },
            productionCountries: productionCountries?.map { $0.toDomain() },
            releaseDate: releaseDate,
            revenue: revenue,
            runtime: runtime,
            spokenLanguages: spokenLanguages?.map { $0.toDomain() },
            status: status,
            tagline: tagline,
            title: title,
            video: video,
            voteAverage: voteAverage,
            voteCount: voteCount
        )
    }
}

extension Genre {
    func toDomain() -> MovieGenre {
        return MovieGenre(id: id, name: name)
    }
}

extension ProductionCompany {
    func toDomain() -> MovieProductionCompany {
        return MovieProductionCompany(id: id, logoPath: logoPath, name: name, originCountry: originCountry)
    }
}

extension ProductionCountry {
    func toDomain() -> MovieProductionCountry {
        return MovieProductionCountry(iso3166_1: iso3166_1, name: name)
    }
}

extension SpokenLanguage {
    func toDomain() -> MovieSpokenLanguage {
        return MovieSpokenLanguage(englishName: englishName, iso639_1: iso639_1, name: name)
    }
}
