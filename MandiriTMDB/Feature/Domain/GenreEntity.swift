//
//  GenreEntity.swift
//  MandiriTMDB
//
//  Created by Benaya Oktavianus on 04/02/24.
//

import Foundation

// MARK: - GenreEntity
struct GenreEntity {
    let id: Int
    let name: String

    // Initializer to convert from API model to Domain entity
    init(from genre: GenreDetailList) {
        self.id = genre.id ?? 0
        self.name = genre.name ?? ""
    }
    
    init(id: Int, name: String) {
        self.id = id
        self.name = name
    }
}

// MARK: - GenreListEntity
struct GenreListEntity {
    let genres: [GenreEntity]

    // Initializer to convert from API model to Domain entity
    init(from genreListResponse: GenreListResponseModel) {
        self.genres = genreListResponse.genres?.map { $0.toDomain() } ?? []
    }
}

extension GenreListResponseModel {
    func toDomain() -> GenreListEntity {
        return GenreListEntity(from: self)
    }
}

extension GenreDetailList {
    func toDomain() -> GenreEntity {
        return GenreEntity(id: id ?? 0, name: name ?? "")
    }
}
