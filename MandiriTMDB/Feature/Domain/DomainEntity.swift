//
//  DomainEntity.swift
//  MandiriTMDB
//
//  Created by Benaya Oktavianus on 03/02/24.
//

import Foundation

struct DiscoverEntity {
    
    static func mapFromDiscoverData(response model: DiscoverResponseModel) -> DiscoverEntity {
        return DiscoverEntity(page: model.page, results: model.results.map({ResultEntity.mapFromResultItem(response: $0)}), totalPages: model.totalPages, totalResults: model.totalResults)
    }
    
    let page: Int
    let results: [ResultEntity]
    let totalPages, totalResults: Int
    
    init(page: Int, results: [ResultEntity], totalPages: Int, totalResults: Int) {
        self.page = page
        self.results = results
        self.totalPages = totalPages
        self.totalResults = totalResults
    }
    
    init() {
        self.page = 0
        self.results = []
        self.totalPages = 0
        self.totalResults = 0
    }
}

struct ResultEntity {
    
    static func mapFromResultItem(response model: ResultItem) -> ResultEntity {
        return ResultEntity(adult: model.adult, backdropPath: model.backdropPath ?? "", genreIDS: model.genreIDS, id: model.id, originalLanguage: model.originalLanguage, originalTitle: model.originalTitle, overview: model.overview, popularity: model.popularity, posterPath: model.posterPath, releaseDate: model.releaseDate, title: model.title, video: model.video, voteAverage: model.voteAverage, voteCount: model.voteCount)
    }
    
    let adult: Bool
    let backdropPath: String
    let genreIDS: [Int]
    let id: Int
    let originalLanguage, originalTitle, overview: String
    let popularity: Double
    let posterPath, releaseDate, title: String
    let video: Bool
    let voteAverage: Double
    let voteCount: Int
    
    init(adult: Bool, backdropPath: String, genreIDS: [Int], id: Int, originalLanguage: String, originalTitle: String, overview: String, popularity: Double, posterPath: String, releaseDate: String, title: String, video: Bool, voteAverage: Double, voteCount: Int) {
        self.adult = adult
        self.backdropPath = backdropPath
        self.genreIDS = genreIDS
        self.id = id
        self.originalLanguage = originalLanguage
        self.originalTitle = originalTitle
        self.overview = overview
        self.popularity = popularity
        self.posterPath = posterPath
        self.releaseDate = releaseDate
        self.title = title
        self.video = video
        self.voteAverage = voteAverage
        self.voteCount = voteCount
    }
}
